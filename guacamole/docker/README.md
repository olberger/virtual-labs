About this container
==================

This container contains Jetty9 (HTTP server) and Guacamole (a XRDP, XVNC & SSH web manager).

To manage your connections, modify ***user-mapping.xml***. Path of this file:

    /usr/share/jetty9/.guacamole/user-mapping.xml


Quick start
==================

##### Launch the container #####

To launch this container, use the following command:

    docker run -td -p 8080:8080 goncalvest/jettyguacamole:tag

To execute the container:

    docker exec -it container_id /bin/bash

##### Access to the container #####

You can access to the login page of Guacamole with your favorite web browser with the link:

    http://your_host_ip:8080/guacamole/#/

The default login is ***username*** and the default password is ***guacamole***.
You can modify the credentials in ***user-mapping.xml***.

References
==================

* A tutorial to install Guacamole with Jetty (outdated):
[Guacamole – RDP/SSH over HTTP][1]
* A tutorial to install Guacamole with Jetty on Raspberry Pi:
[Installing Guacamole on Raspberry Pi][2]
* [Apache Guacamole][3]

[1]: https://desaille.fr/guacamole-rdpssh-over-http/
[2]: http://www.m-opensolutions.com/?p=936
[3]: https://guacamole.apache.org/