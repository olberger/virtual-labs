Provision-script.sh from https://github.com/inframate/Vagrant-Guacamole/blob/master/Provision-script.sh

# Guacamole running inside a Vagrant VM

This is a Vagrant VM running :
- a CentOS 7 host (`server`) running Guacamole;
- a CentOS 8 host (`desktop`) running a graphical (Gnome) desktop,
  ready to install the MeshCentral agent
- a Windows 10 host (`windows`) running Windows 10 + Edge, ready to
  install the MeshCentral agent


Refer to : https://github.com/olberger/Vagrant-Guacamole


