######################
# Install Labtainers #
######################

# Install Docker from https://docs.docker.com/install/linux/docker-ce/debian/

apt-get update
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io

# Get start-labtainer file at https://gitlab.com/olberger/virtual-labs

cd /
wget https://gitlab.com/olberger/virtual-labs/raw/master/labtainer-docker/start-labtainer
chmod 755 start-labtainer
