#!/bin/bash


USER=${USER:-root}
HOME=/root
if [ "$USER" != "root" ]; then
    echo "* enable custom user: $USER"
    useradd --create-home --shell /bin/bash --user-group --groups adm,sudo $USER
    if [ -z "$PASSWORD" ]; then
        echo "  set default password to \"ubuntu\""
        PASSWORD=thepassword
    fi
    HOME=/home/$USER
    echo "$USER:$PASSWORD" | chpasswd
#    cp -r /root/{.gtkrc-2.0,.asoundrc} ${HOME}
#    [ -d "/dev/snd" ] && chgrp -R adm /dev/snd
fi
#sed -i "s|%USER%|$USER|" /etc/supervisor/conf.d/supervisord.conf
#sed -i "s|%HOME%|$HOME|" /etc/supervisor/conf.d/supervisord.conf

# home folder
#mkdir -p $HOME/.config/pcmanfm/LXDE/
#ln -sf /usr/local/share/doro-lxde-wallpapers/desktop-items-0.conf $HOME/.config/pcmanfm/LXDE/
chown -R $USER:$USER $HOME

VNC_PASSWORD=${VNC_PASSWORD:-toto123}
if [ -n "$VNC_PASSWORD" ]; then
    # echo -n "$VNC_PASSWORD" > /.password1
    # x11vnc -storepasswd $(cat /.password1) /.password2
    # chmod 400 /.password*
    # sed -i 's/^command=x11vnc.*/& -rfbauth \/.password2/' /etc/supervisor/conf.d/supervisord.conf
    mkdir -p $HOME/.vnc/
    echo "$VNC_PASSWORD" | /usr/bin/vncpasswd -f > $HOME/.vnc/passwd 
    chmod 664 $HOME/.vnc/passwd

    sed -i "s/toto123/$VNC_PASSWORD/" /etc/guacamole/user-mapping.xml

    export VNC_PASSWORD=
fi

GUACAMOLE_USER=${GUACAMOLE_USER:-guacuser}
if [ -n "$GUACAMOLE_USER" ]; then
    GUACAMOLE_PASSWORD=${GUACAMOLE_PASSWORD:-guacpass}

    sed -i "s/guacuser/$GUACAMOLE_USER/" /etc/guacamole/user-mapping.xml
    PASSWORD=$(echo -n "$GUACAMOLE_PASSWORD" | md5sum | cut -d " " -f1)
    sed -i "s/6e56865d57e0d6cef4303c0687c7d549/$PASSWORD/" /etc/guacamole/user-mapping.xml

    export GUACAMOLE_PASSWORD=
fi

sed -i "s|/root/.vnc/passwd|$HOME/.vnc/passwd|" /etc/supervisor/conf.d/supervisord.conf

# if [ -n "$X11VNC_ARGS" ]; then
#     sed -i "s/^command=x11vnc.*/& ${X11VNC_ARGS}/" /etc/supervisor/conf.d/supervisord.conf
# fi


exec /bin/tini -- /usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf
