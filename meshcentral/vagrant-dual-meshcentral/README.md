# MeshCentral2 running inside a Vagrant VM

This is a Vagrant VM running :
- a CentOS 7 host (`server`) running MeshCentral2;
- a CentOS 8 host (`desktop`) running a graphical (Gnome) desktop,
  ready to install the MeshCentral agent
- a Windows 10 host (`windows`) running Windows 10 + Edge, ready to
  install the MeshCentral agent


# Usage

Simply clone this repo, and run `vagrant up`:

```
$ vagrant up server
```

Then connect to `https://localhost:8443/` on your host, to load the
MeshCentral server's Web UI. Acknowledge the security risk warning, to
load the page, and you have it.

You then have to, for instance :
- create a new admin user
- login
- create a new group
- add a new agent, and select "Linux / BSD"
- then copy the script snippet which looks like:
```
(wget "https://localhost/meshagents?script=1" --no-check-certificate -O ./meshinstall.sh || wget "https://localhost/meshagents?script=1" --no-proxy --no-check-certificate -O ./meshinstall.sh) && chmod 755 ./meshinstall.sh && sudo -E ./meshinstall.sh https://localhost 'L0iyvvrYkWKZ00Jhx4GoUiyNW$OP3XqaFlLw4w8bbGUTd67gWp@MD@adcGiK3ONg' || ./meshinstall.sh https://localhost 'L0iyvvrYkWKZ00Jhx4GoUiyNW$OP3XqaFlLw4w8bbGUTd67gWp@MD@adcGiK3ONg'
```
- note the hash provided there, here it would be: `'L0iyvvrYkWKZ00Jhx4GoUiyNW$OP3XqaFlLw4w8bbGUTd67gWp@MD@adcGiK3ONg'`
- SSH to the VM using `vagrant ssh`
- then type: `sudo -E ./meshinstall.sh https://localhost 'L0iyvvrYkWKZ00Jhx4GoUiyNW$OP3XqaFlLw4w8bbGUTd67gWp@MD@adcGiK3ONg'`

This will install the Linux MeshCentral agent on the VM running MeshCentral (we already installed the agent's installation script `meshinstall.sh` at Vagrant provisionning).

This will result in the appearance of the `localhost.localdomain` agent in the group, in the MeshCentral Web interface.

The interface should then allow you to connect as root on the VM itself, from within the Web interface (right click on the `localhost.localdomain` icon and select "Terminal").

