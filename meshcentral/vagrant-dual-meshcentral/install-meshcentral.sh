#!/usr/bin/env bash

sudo yum install --assumeyes --quiet \
     wget

curl -sL https://rpm.nodesource.com/setup_10.x | bash -
yum install -y nodejs

mkdir -p /home/vagrant/meshcentral-data

cp /vagrant/package.json /home/vagrant/

cp /vagrant/config.json /home/vagrant/meshcentral-data/

npm install meshcentral --loglevel verbose

chown -R vagrant:vagrant /home/vagrant/meshcentral-data/

sudo cp /vagrant/meshcentral.service /etc/systemd/system/meshcentral.service

sudo systemctl enable meshcentral.service
sudo systemctl start meshcentral.service

wget https://localhost/meshagents?script=1 --no-check-certificate -O ./meshinstall.sh
sudo chown vagrant:vagrant ./meshinstall.sh

echo "meshcentral should be available on https://localhost:8443/"

